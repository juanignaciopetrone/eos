$(document).ready(() => {
  $('.js-icons-headers').toggleClass('hidden')

  $('.js-icons-search')
    .on('change keyup paste', function () {
      const value = $(this).val().toLowerCase()
      /* Perform the filtering */
      $('.icons-list article').hide().filter(function () {
        return $(this).text().toLowerCase().indexOf(value) > -1
      }).show()
      /* Show or hide the "no results" text */
      $('.js-icons-search-empty').toggleClass('hidden', $('.icons-list article:visible').length > 0)
      $('.js-icons-headers').toggleClass('hidden', $('.icons-list article:visible').length === 0)
    })

  $('.js-icons-search-close').on('click', function () {
    $('.js-icons-search').val('').change()
  })
})
