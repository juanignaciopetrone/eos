const getDesignSpecs = (callback) => { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `https://eos-strapi.herokuapp.com/graphql?query={designspecs{name,tag,xdSpecs,xdBackup,trello,documentDescription,date}}`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`There was an error in the request: ${error}`)
      }
    }))
    .then(function (data) {
      callback(data)
    })
}
