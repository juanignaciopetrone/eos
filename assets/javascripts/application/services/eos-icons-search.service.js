function searchIconsService (query, callback) { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `/api/icons/${query}`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the icon you are looking for`)
        $('.js-eos-icons-notification').show().text(`There are no icons matching your search criteria. Try with a different word.`)
      }
    }))
    .then(function (iconsCollection) {
      callback(iconsCollection)
    })
}
