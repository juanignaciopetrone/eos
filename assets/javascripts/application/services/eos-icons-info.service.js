const getMoreInfoFromIconService = (icon, callback) => { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: `/javascripts/application/models/eos-icons/${icon}.json`,
      dataType: 'json',
      error: function (xhr, status, error) {
        console.log(`there was an error retrieving the icon information`)
        callback()
      }
    }))
    .then(function (data) {
      callback(data)
    })
}
