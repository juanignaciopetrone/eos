/* Tooltip initializer for .js-tooltip
   ========================================================================== */
$(function () {
  $('.js-tooltip').mouseover(function () {
    $(this).tooltip('show')
    $(this).mouseout(function () {
      $(this).tooltip('hide')
    })
  })
})
