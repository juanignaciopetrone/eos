/* ==========================================================================
  Design specs
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
let _designSpecsContainer, designSpecsItemTemplate

$(function () {
  // Prepare design specs containers
  _designSpecsContainer = $('.js-design-specs-list')
  designSpecsItemTemplate = $('.js-design-specs-item').clone(true)
  $('.js-design-specs-item').remove()

  // Initiate the full collection of design specs collection
  designSpecsCollection()
})

const designSpecsCollection = () => {
  getDesignSpecs(_data => { // eslint-disable-line no-undef
    /* Reverse data to show new entry first */
    const designSpecsData = _data.data.designspecs

    // sort by date
    const sortByDate = (a, b) => {
      return new Date(a.date).getTime() - new Date(b.date).getTime()
    }

    designSpecsData.sort(sortByDate).reverse()

    /* RegEx to get the date without hour */
    const regEx = /[0-9]*-[0-9]*-[0-9]*/gm

    for (let i = 0; i < designSpecsData.length; i++) {
      const newDesignSpecsItem = designSpecsItemTemplate.clone(true)
      const createdDate = designSpecsData[i].date.match(regEx)
      // Add design specs info
      $(newDesignSpecsItem).find('.js-design-specs-name').text(designSpecsData[i].name)
      $(newDesignSpecsItem).find('.js-design-specs-date').text(createdDate)

      $(newDesignSpecsItem).find('.js-design-specs-tag').text(designSpecsData[i].tag)
      $(newDesignSpecsItem).find('.js-design-specs-xd-specs').attr('href', `${designSpecsData[i].xdSpecs}`)
      $(newDesignSpecsItem).find('.js-design-specs-xd-backup').attr('href', `${designSpecsData[i].xdBackup}`)
      $(newDesignSpecsItem).find('.js-design-specs-trello').attr('href', `${designSpecsData[i].trello}`)
      $(newDesignSpecsItem).find('.js-design-specs-description').text(designSpecsData[i].documentDescription)

      $(_designSpecsContainer).append(newDesignSpecsItem)
    }
  })
}
