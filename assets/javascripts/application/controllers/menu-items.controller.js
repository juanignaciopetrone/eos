$(function () {
  /*
    Use this as follows:
    Add the class .js-select-current to the link you want to select
    it will check if `href = path` matches with the current path
    in the URL, and if it does, the item will get the class `.selected`
   */
  $('.js-select-current').addClass(function () {
    const itemRoute = $(this).attr('href')
    return window.location.pathname === itemRoute ? 'selected' : ''
  })

  /*
    Use this as follows:
    Add the class .js-select-current-parent to the link you want to select.
    It will check if the `href = path` in your item
    matches with parent section of the current path. For example:
    being located at localhost:3000/foo/bar, if your href is `/foo`
    the item will get the class `.selected`, but if the href was `/foo/bar`
    then it will be skipped and not be selected.
   */
  $('.js-select-current-parent').addClass(function () {
    const itemRoute = $(this).attr('href').replace('/', '')
    const currentRoute = window.location.pathname
    const currentRouteParent = currentRoute.split('/')
    return itemRoute === currentRouteParent[1] ? 'selected' : ''
  })

  /*
    Use this as follows:
    Add the class .js-feature-flag to an item to want to verify with
    feature flag.
    You should also include the data attribute feature-flag like
    `data-feature-flag= docEnabled['/path/of-feature']`.
    If the feature is enabled it will be shown, if not, it will get the
    `hidden` class. Features undefined will still be shown as it is assumed
    there is some missing data from the DB but the feature should be Feature Flagged
   */
  $('.js-feature-flag').addClass(function () {
    const ff = $(this).data('feature-flag')
    if (ff !== undefined) {
      return ff.enabled ? '' : 'hidden'
    }
    return false
  })
})
