const express = require('express')
const router = express.Router()
const fs = require('fs')
const fetch = require('node-fetch')
const configData = JSON.parse(fs.readFileSync('./config.json'), 'utf8')
const markdown = require('marked')

router.get('/:article', async (req, res, next) => {
  /* Query news */
  const newsQuery = `news { title thumbnail{ url } subtitle content date }`
  const newsRequest = await strapiGraphql(newsQuery)

  /* Get the response and reduce to new object with the path as key, this way we can check if the req.parms match any path */
  const newsResponse = newsRequest.data.news.reduce((acc, cur) => {
    const route = symbolsParser(cur.title)
    /* acc = accumulator , cur = current item */
    acc[route] = {
      articleTitle: cur.title,
      articleSubtitle: cur.subtitle,
      articleContent: cur.content,
      articleDate: cur.date
    }

    return acc
  }, {})

  /* Check if the parms match any new */
  if (newsResponse[req.params.article]) {
    /* Get the needed data from the object */
    const { articleTitle, articleSubtitle, articleDate, articleContent } = newsResponse[req.params.article]

    /* Transorm the date */
    const regEx = /[0-9]*-[0-9]*-[0-9]*/gm
    const useDate = articleDate.match(regEx)

    res.locals.docTitle = articleTitle
    res.locals.docDescription = articleSubtitle
    res.locals.docPath = req.originalUrl

    /* Send data to the view */
    res.render('news', {
      articleTitle,
      articleSubtitle,
      path: req.originalUrl,
      articleDate: useDate,
      articleContent,
      markdown
    })
  } else {
    res.send('Not found')
  }
})

/* Converte the new title so we can use as route */
const symbolsParser = (str) => {
  const regEx = /(\w+)/g
  return str.toLowerCase().match(regEx).join('-')
}

/* Fetch Strapi for data */
const strapiGraphql = async (query) => {
  const { strapiURL } = configData

  /* Fetch graphql with the query */
  const request = await fetch(`${strapiURL}/graphql?query={${query}}`)
  const data = request.json()

  return data
}

module.exports = router
